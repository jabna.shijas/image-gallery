const scrollContainer = document.querySelector('.gallery');
const backBtn = document.getElementById('backBtn');
const nextBtn = document.getElementById('nextBtn');

// Handle mouse wheel scroll
scrollContainer.addEventListener("wheel", (event) => {
   // event.preventDefault();
    scrollContainer.scrollLeft += event.deltaY;
    scrollContainer.style.scrollBehaviour="smooth";
});

// Handle previous button click
backBtn.addEventListener('click', () => {
    scrollContainer.style.scrollBehaviour="smooth";
    scrollContainer.scrollLeft -= scrollContainer.clientWidth;
});

// Handle next button click
nextBtn.addEventListener('click', () => {
    scrollContainer.style.scrollBehaviour="smooth";
    scrollContainer.scrollLeft += scrollContainer.clientWidth;
});
